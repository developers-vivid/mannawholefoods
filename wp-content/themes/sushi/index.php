<?php
/*
* Theme's Front Page.
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$query = new WP_Query();	
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="content-area">
			<div class="main-container">
				<div class="slider-holder left">
					<div id="rotator" class="round-border">
						<div class="rotator-wrapper">
							<?php
								$tpath = get_bloginfo( "template_url" );
								$slider_query = new WP_Query();
								// get posts in 'slider' category
								$sliders = $slider_query->query( array(
									"category_name" => "slider",
									"orderby" => "ID",
									"order" => "ASC",
									"post_type" => "post",
									"post_status" => "publish"
								));
								
								if ( $sliders ) :
								
									foreach ( $sliders as $slider ) :
									
										if ( has_post_thumbnail( $slider->ID ) )
										{
											// featured image
											$image = get_post( get_post_thumbnail_id( $slider->ID ) );
											// ensure image path only
											$image = $image->guid;
										}
										else
										{
											/* $image = alternate image if no thumbnail is found */
										}
										
										// title / heading
										$title = $slider->post_title;
										// description
										$desc = apply_filters( "the_content", $slider->post_content );
										// alternate text from custom field "Alt Text"
										$alttext = get_post_meta( $slider->ID, "Alt Text", true );						
									
							?>

								<div class="slide">
									<img src="<?php set_timthumb( $tpath, $image, 833, 510 ); ?>" alt="<?php echo $slider->post_content; ?>" title="<?php echo $slider->post_content; ?>" />
								</div>
								

						<?php 	endforeach; ?>
					<?php endif; ?>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<?php get_sidebar(); ?>

				<article class="main-entry">
					<?php
						$content = get_post( $id = 85 );
						$subcontent = get_post_meta(85,'subcontent', true);
					?>
					
					<img src="<?php bloginfo('template_url');?>/images/manna-graphic.png" alt="Manna Wholefoods Organic and Natural grocer" title="Manna Wholefoods Organic and Natural grocer"/>
					<div class="line"></div>
					
					<p class="main-content">
						<?php _e( $content->post_content )?>
					</p>
					<p class="main-content-two"><?php _e( $subcontent );?></p>
				</article>
				<!-- /main-content -->
				<br class="clr" />
				<div id="content-bottom">
					<h2 class="featured-products">FEATURED PRODUCTS AND SERVICES</h2>
					<!--<a href="<?php echo site_url();?>/products-and-services/" class="views right" target="_blank">View All</a>-->
					<div class="clr"></div>
				</div>
					<ul id="featured-images">
						<?php

							$tpath = get_bloginfo( "template_url" );

							$featured_query = new WP_Query();
							$featured = $featured_query->query( array(
									"category_name" => "featured",
									"orderby" => "ID",
									"order" => "ASC",
									"post_type" => "post",
									"post_status" => "publish"
								));

							if ( $featured ) :
								
									foreach ( $featured as $featured ) :
									
										if ( has_post_thumbnail( $featured->ID ) )
										{
											// featured image
											$image = get_post( get_post_thumbnail_id( $featured->ID ) );
											// ensure image path only
											$image = $image->guid;
										}
										else
										{
											/* $image = alternate image if no thumbnail is found */
										}
										
										// title / heading
										$title = $featured->post_title;
										// description
										$desc = apply_filters( "the_content", $featured->post_content );
										// alternate text from custom field "Alt Text"
										$alttext = get_post_meta( $featured->ID, "Alt Text", true );						
										
										$links = get_post_custom_values('link', $featured->ID );
										
										foreach( $links as $link ){
							?>
					
						<li>
							
							<a href="<?php _e( $link );?>">
								<img src="<?php set_timthumb( $tpath, $image, 272, 181 ); ?>" alt="<?php echo $alttext; ?>" title="<?php echo $title; ?>" />
								<p><?php _e( $title );?></p>
							</a>
						</li>
						<?php }
							endforeach; ?>
					<?php endif; ?>

					</ul>
				<div class="articles left">
					<h3 class="left">Latest Articles</h3><a href="<?php _e( get_permalink( 283 ) );?>" class="readmore">View All Articles</a>
					<br class="clr" />
					<div class="wrapper">
						<p class="intro"><?php 
						wp_reset_query();
						$content = get_page( $id = 690 );
							_e( $content->post_content );?></p>
						<ul>	
						<?php
							
							$args = $query->query(array(
											'category_name'		=> 'Latest Articles',
											'post_type'			=> 'post',
											'order'				=> 'desc',
											'orderby'			=> 'date',
											'posts_per_page'	=> 7
											
							));
								// echo "<pre>";
								// print_r($args);
								// echo "</pre>";	
							foreach($args as $list):
								$links = get_post_meta($list->ID, 'link', true );
								if($links == '' || empty($links)){
									$link = get_permalink( $list->ID );
								}else{
									$link = $links;
								}
								//foreach($links as $link){
							?>
								<li><a href="<?php _e( $link );?>"><?php _e( $list->post_title );?></a></li>	
							<?php
								//}
							endforeach;
						?>
						</ul>
					</div>
				</div>
				<div class="resident right">
					<?php
					$id = 1134; //Live
					//$id = 1150; //Demo
					$link = get_post_meta( $id, 'link', true );?>
					<a href="<?php _e( $link );?>">
						<?php $image_url = get_post( get_post_thumbnail_id( $id ) ); ?>
						<img src="<?php set_timthumb( $tpath, $image_url->guid, 560, 342 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
					</a>
					<!-- <h3>Resident Naturopath</h3>
					<div class="wrapper">
						<div class="image-holder left">
							<?php
								$tpath = get_bloginfo( "template_url" );
								$image_url = get_post( get_post_thumbnail_id( 120 ) );
								$content = get_post( $ids = 120);
								$landline2 = get_post_meta(120,'landline2',true);
							?>
							<img src="<?php set_timthumb( $tpath, $image_url->guid, 177, 239 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
						</div>
						<p>
							<span><?php _e( $content->post_title );?></span><br />
							<?php _e( $content->post_content );?>
						</p>
						<div class="direct-contact">
							<div class="information"><p>
								Call<br />
								<span class="information-number"><?php _e($landline2)?></span><br />
								to make an appointment
							</p></div>
							<div class="clr"></div>
						</div>
						<div class="clr"></div>
					</div> -->
				</div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</section>
		<div class="clr"></div>
		<!-- /content-area -->

<?php get_footer(); ?>