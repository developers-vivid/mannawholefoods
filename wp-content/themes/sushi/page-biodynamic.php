<?php
/*
* Template Name: Biodynamic
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 24 );
					//$contentlist = get_post_meta(103,'contentlist', false);
					$subcontent = get_post_meta(24, 'subcontent', true);
					$subcontent2 = get_post_meta(24, 'subcontent2', true);
					$subcontent3 = get_post_meta(24, 'subcontent3', true);
					$usefulwebsites = get_post_meta(24,'usefulwebsites', true);
					$usefulwebsites2 = get_post_meta(24,'usefulwebsites2', true);

				?>
				<h2><?php _e($content->post_title)?></h2>
				<div class="inner-image-holder">
					<?php
						$tpath = get_bloginfo( "template_url" );
						$image_url = get_post( get_post_thumbnail_id( 24 ) );
						$content = get_post( $id = 24);
					?>
					<img src="<?php set_timthumb( $tpath, $image_url->guid, 425, 282 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
				</div>
					<p class="main-content-two">
						<?php _e($content->post_content)?>
					</p>
					<p class="style-italic">
						<?php _e($subcontent)?>
					</p>
					<p class="main-content-three">
						<?php _e($subcontent2)?>
					</p>
					<div class="clr"></div>
					<ul class="list">
						<strong>Useful links:</strong>
						<li><a href="<?php _e($usefulwebsites)?>"><?php _e($usefulwebsites)?></a></li>
						<li><a href="<?php _e($usefulwebsites2)?>"><?php _e($usefulwebsites2)?></a></li>
					</ul>
					<div class="clr"></div>
					<ul class="list">
						<?php _e($subcontent3)?>
					</ul>
				</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>