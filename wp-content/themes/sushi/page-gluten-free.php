<?php
/*
* Template Name: Gluten Free
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 18 );
					$usefulwebsites = get_post_meta(18,'usefulwebsites', true);
				?>
				<h1><?php _e($content->post_title)?></h1>
				<p class="main-content-two">
					<?php _e($content->post_content)?>
					<a href="<?php _e($usefulwebsites);?>" target="_blank" class="green"><?php _e($usefulwebsites);?></a>
					<?php
							$informationlist = get_post_meta(18, 'informationlist', true);
					?>
						
						<?php
							/* $informationlist = get_post_meta(18, 'informationlist', true);
							$informationlist = explode('|', $informationlist);
							
						if( !empty($informationlist) ) :  */
						?>
						<!-- 	<ul class="main-content-three">
							<b>For more information on this subject, refer to the following books available<br />in the book section at Manna Wholefoods:</b><br /> -->
							<?php //_e( apply_filters('the_content', $description) );
								//foreach ($informationlist as $value) : ?>
								<!-- 	<li><?php _e($value) ?></li> -->

								<?php// endforeach; ?>

						<!-- 	</ul> -->
						<?php// endif; ?>
				</p>
				
				<div class="clr"></div>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>