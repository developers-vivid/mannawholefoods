<?php
/*
* Theme's Default Theme
* Template Name: Default
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
			
				<h1><?php $pageDefault = get_page(get_the_id()); echo $pageDefault->post_title; ?></h1>
				<div class="content-area-text">
					<div class="featured-image">
					<?php 
					if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						the_post_thumbnail( 'full' );
					}
					?>
					</div>
					<?php echo apply_filters('the_content', $pageDefault->post_content); ?>
				</div>
				
				<div class="clr"></div>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>