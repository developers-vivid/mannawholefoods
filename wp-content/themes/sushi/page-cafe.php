<?php
/*
* Template Name: Cafe
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
	<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 6 );
					//$contentlist = get_post_meta(103,'contentlist', false);
				?>
				<h1><?php _e($content->post_title)?></h1>
				<div class="inner-image-holder">
					<?php
						$tpath = get_bloginfo( "template_url" );
						$image_url = get_post( get_post_thumbnail_id( 6 ) );
						$content = get_post( $id = 6);
					?>
					<img src="<?php set_timthumb( $tpath, $image_url->guid, 789, 284 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
				</div>
						<?php
							$listcontent = get_post_meta(6, 'listcontent', true);
						?>
						
						<?php
							$listcontent = get_post_meta(6, 'listcontent', true);
							$listcontent = explode('|', $listcontent);
							
						if( !empty($listcontent) ) : ?>
							<ul class="gourmet">
							<?php _e($content->post_content);?>
							<?php //_e( apply_filters('the_content', $description) );
								foreach ($listcontent as $value) : ?>
									<li><p><?php _e($value) ?></p></li>

								<?php endforeach; ?>

							</ul>
						<?php endif; ?>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>