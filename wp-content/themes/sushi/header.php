<?php
/*
* The header of the theme.
*
* @package WordPress
* @subpackage sushi
*/
global $sushi;
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title><?php get_page_title(); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=1200"  />

<?php wp_head(); ?>
<?php $sushi->getSiteVerificationMeta(); ?>

<link rel="stylesheet" type="text/css" media="screen" href="<?php _e( $sushi->stylesheetURL . $sushi->getTimeSuffix() ); ?>" />
<link rel="shortcut icon" type="image/x-icon" href="<?php _e( $sushi->templateURL ); ?>/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="<?php _e( $sushi->templateURL ); ?>/favicon.png" />

<script type="text/javascript" src="<?php _e( $sushi->templateURL ); ?>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php _e( $sushi->templateURL ); ?>/js/jquery-plugins/jquery.kai-kaitools-beta.0.1.min.js"></script>
<script type="text/javascript" src="<?php _e( $sushi->templateURL ); ?>/js/jquery-plugins/slides.min.jquery.js"></script>
<script type="text/javascript" src="<?php _e( $sushi->templateURL ); ?>/js/jquery-plugins/jquery.sushi.min.js"></script>

<!--[if lt IE 9]>
<script src="<?php _e( $sushi->templateURL ); ?>/js/html5shiv.js"></script>
<style type="text/css">
	* {	*behavior: url( "<?php _e( $sushi->templateURL ); ?>/css/boxsizing.htc" ); }
</style>
<![endif]-->

</head>
<?php flush(); ?>
<body <?php body_class(); ?>>
<div id="main-wrapper">
	<header>
			<div class="main-container">
				<nav id="main-nav">
					<?php wp_nav_menu( array( 'menu' => 'Main Menu', 'container_id' => 'main-nav') ); ?>
				<div class="clr"></div>
			</nav>
			<!-- /main-nav -->
		</div>
		<div class="clr"></div>
		<!-- /main-container-->
		</header>
		<div id="sub-header">
			<div class="main-container">
				<div id="logo"><a href="<?php bloginfo( "home" ); ?>">
					<img src="<?php _e( $sushi->templateURL ); ?>/images/logo.png" alt="Manna Wholefoods &amp; Cafe Fremantle" title="Manna Wholefoods &amp; Cafe Fremantle" />
				</a>
			</div>
			<?php
				
				$address = get_post_meta(65,'address', true);
				$email = get_post_meta(65,'email', true);
				$facebook = get_post_meta(65,'facebook', true);
				$fax = get_post_meta(65,'fax', true);
				$landline = get_post_meta(65,'landline', true);
				$content = get_post( $id = 65 );

				$facebook = get_field('facebook', 1438);
				$instagram = get_field('instagram', 1438);

				//thi is located on post under ID 65
			?>	
			<div class="caption">
				<p><img src="<?php bloginfo('template_url');?>/images/opening-phrase-1.png" title="Manna Wholefoods Organic and Natural grocer" alt="Manna Wholefoods Organic and Natural grocer" /></p>
			</div>	
			<!--<p><?php _e($content->post_content);?></p></div>-->
			<div class="contact right">

				<!-- <a href="<?php //_e($facebook);?>" class="fb-icon" target="_blank"> -->
					<!-- <img src="<?php //_e( $sushi->templateURL ); ?>/images/facebook.png" title="like us on facebook" alt="facebook"> -->
				<!-- </a> -->
				
				<div class="social-media clearfix">
					<?php if ( $instagram || $facebook ): ?>
					<ul>
						<?php echo ( $facebook ? '<li><a href="'.$facebook.'" target="_blank"><img src="'.get_template_directory_uri().'/images/fb-icon.png" title="Like us on Facebook"></a></li>' : '' ); ?>
										<?php echo ( $instagram ? '<li><a href="'.$instagram.'" target="_blank"><img src="'.get_template_directory_uri().'/images/ig-icon.png" title="Like us on Instagram"></a></li>' : '' ); ?>
					</ul>
					<?php endif ?>
					<!-- <br /> -->
					<!-- <div class="clr"></div> -->
					<p class="contact-us-button"><a href="contact-us">Find Us</a></p>
				</div>

				<div class="clr"></div>
				<p class="details"><?php _e($address);?></p>
			
			<div class="clr"></div>
			</div>
		</div>
		<div class="clr"></div>
		<!-- /header -->
