<?php
/**
 * The template for displaying Search Results pages.
 *
 * package WordPress
* @subpackage sushi
 */
?>
<?php get_header(); ?>
	<div id="about-us">
		<div class="text-content left">		
			<?php if ( have_posts() ) :?>
				<h1 class="search"><?php printf( __( 'Search Results for: %s', 'sushi' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				<?php while ( have_posts() ) : the_post(); ?>
				<p>
					<?php 
						echo '<b><a href="'.get_permalink(get_the_ID()).'">';
						the_title();
						echo '</a></b>';
						echo the_content(); 
					?>
				</p>
				<?php endwhile; else: ?>
				<h1 class="search"><?php _e( 'Nothing Found', 'sushi' ); ?></h1>
				<p><?php _e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.'); ?></p>
			<?php endif; ?>
		<!-- /main-content -->
		</div>
	</div>
		<!-- /content-area -->
	
<?php get_footer(); ?>