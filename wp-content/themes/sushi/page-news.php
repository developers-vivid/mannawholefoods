<?php
/*
* Template Name: News
*
* @package WordPress
* @subpackage Sushi
*/
?>

<?php
	global $wpdb, $post;
	$pageid = get_id();
	$data = get_page( $pageid );
	$custom_posts = get_posts( array(	"post_status" => "publish",'post_type' => 'page',"posts_per_page" => -1, "post_parent" => $pageid	) );

	$tpath = get_bloginfo( "template_url" );
	remove_filter( "the_content", "wpautop" );
	$content = apply_filters( "the_content", $data->post_content );
?>
<?php get_header(); ?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				
				<h1><?php _e($data->post_title)?></h1>

				<?php 
				
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$paged_news = new WP_Query( array('posts_per_page' => 4, 'paged' => $paged, 'category_name' => 'article-recipes', 'order' => 'desc', 'post_status' => 'publish', 'post_type' => 'post' ) );				

				 ?>
				<?php 
				while ($paged_news->have_posts()) : $paged_news->the_post(); ?>
					
				<div class="per-post">
				<?php if ( has_post_thumbnail() ) { ?>
					
						<?php						
							$image_url = get_post( get_post_thumbnail_id( get_the_ID() ) );
							$content = get_post( get_the_ID() );
						?>
						<img src="<?php set_timthumb( $tpath, $image_url->guid, 215, 208, 100, 1 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>" class="left"/>
					
				<?php } ?>
					<h2><?php the_title(); ?></h2>
					<p class="main-content-four">
						<?php
							_e(displaySubStringWithStrip($post->post_excerpt, 230));
							
							$true = get_post_custom_values('new-tab', $post->ID);
							
							foreach($true as $t):
							
									$readmore = get_post_custom_values('readmore', $post->ID );
					
									foreach($readmore as $more):
										
										if($t === "yes" || $t === "Yes" || $t === "YES" ){		
										?>
											<a href="<?php _e( $more ); ?>" target="_blank">Read More</a>
										<?php	
										}else if($t === "No" || $t === "NO" || $t === "no"){
										?>
											<a href="<?php _e( $more ); ?>">Read More</a>
										<?php	
										}
									endforeach;
							endforeach;
							
							?>
							<!--<a href='<?php //_e(get_bloginfo("home") . "/article-recipes/" . $post->post_name); ?>' target="_blank">Read More</a>-->
							
					</p>
					<div class="clr"></div>
				</div>
				<?php endwhile; ?>
				<div class="clr"></div>
				
				<div class="pagination-news">
						<?php 
							 $args = array(
								'base' => @add_query_arg('paged','%#%'),
								'format' => '?paged=%#%',
								'current' => max( 1, get_query_var('paged') ),
								'prev_text'    => __('Previous'),
								'next_text'    => __('Next'),
								'total' => $paged_news->max_num_pages
							);
					echo paginate_links( $args );  ?>
				</div> 
				
			</article>
			<!-- /main-content -->
			<?php get_sidebar('left'); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>