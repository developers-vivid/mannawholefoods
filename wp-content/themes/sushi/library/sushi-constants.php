<?php
/**
 * Constants
 */
define( 'WP_OPTION_COPYRIGHT', 				'sushi_copyright' );
define( 'WP_OPTION_SITE_BY', 				'sushi_site_by' );
define( 'WP_OPTION_OFFICIAL_SITE', 			'sushi_official_site' );
define( 'WP_OPTION_GA_ACCOUNT', 			'sushi_ga_account' );
define( 'WP_OPTION_GOOGLE_VERIFICATION', 	'sushi_google_verification' );
define( 'WP_OPTION_BING_VERIFICATION', 		'sushi_bing_verification' );
 
?>