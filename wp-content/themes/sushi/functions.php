<?php
/*
* Theme's functions [v2.1].
*
* @package WordPress
* @subpackage sushi
*/

/**
 * Sushi Library
 */
require_once( "library/sushi.php" );
global $sushi;

/**
 * Tell WordPress to run sushi_theme_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'sushi_theme_setup' );
add_filter( 'show_admin_bar', '__return_false' );

if ( ! function_exists( "sushi_theme_setup" ) ):
/**
 * Setup the sushi theme.
 */
function sushi_theme_setup()
{
	// Drop this in functions.php or your theme
	if ( !is_admin())
	{
		wp_deregister_script('jquery');		
	}	
	sushi_main_nav();
}
endif;

// Load up our theme options page and related code.
require( get_template_directory() . '/library/includes/theme-options.php' );

// Add support for custom backgrounds.
add_theme_support( 'custom-background' );

// Add support for custom headers.
add_theme_support( 'custom-header', array(
	// Header image default
	'default-image'			=> get_template_directory_uri() . '/images/headers/default.jpg',
	// Header text display default
	'header-text'			=> false,
	// Header text color default
	'default-text-color'		=> '000',
	// Header image width (in pixels)
	'width'				=> 1000,
	// Header image height (in pixels)
	'height'			=> 198,
	// Header image random rotation default
	'random-default'		=> false,
	// Template header style callback
	'wp-head-callback'		=> $wphead_cb,
	// Admin header style callback
	'admin-head-callback'		=> $adminhead_cb,
	// Admin preview style callback
	'admin-preview-callback'	=> $adminpreview_cb
) );

if ( ! function_exists( "sushi_main_nav" ) ):
/**
 * Navigation registry.
 */
function sushi_main_nav()
{
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'MainNav', 'sushi' ) );
	register_nav_menu( 'footer', __( 'FooterNav', 'sushi' ) );
}
endif;

if ( ! function_exists( "sushi_custom_sidebars" ) ):
/**
 * Registers custom sidebars
 */
function sushi_custom_sidebars( $custom )
{
	register_sidebar( $custom );
}
endif;

if ( ! function_exists( "get_postdata" ) ):

function get_postdata( $id )
{	
	global $wpdb;
	
	$sql = "SELECT $wpdb->posts.* FROM $wpdb->posts WHERE $wpdb->posts.ID=$id AND $wpdb->posts.post_type='post'";
	$postdata = $wpdb->get_row( $sql );
		
	return $postdata;
}
endif;

if ( ! function_exists( "set_timthumb" ) ):

function set_timthumb( $path, $src, $width, $height, $quality = 100, $zc = 1 )
{	
	_e( esc_url( $path, '') . "/timthumb.php?src=$src&w=$width&h=$height&q=$quality&zc=$zc" );
}
endif;

if ( ! function_exists( "_pr" ) ):

function _pr( $mixed )
{	
	print( "<pre>" );
	print_r( $mixed );
	print( "</pre>" );
}
endif;

/*Strip string*/
function displaySubStringWithStrip($string, $length=NULL)
{
    if ($length == NULL)
            $length = 50;
  
    $stringDisplay = substr(strip_tags($string,'<br><a>'), 0, $length);

    if (strlen(strip_tags($string,'<br><a>')) > $length)
        $stringDisplay .= '...';
    return $stringDisplay;
}
/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

// Add support for Featured Images
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    add_image_size('index-categories', 150, 150, true);
    add_image_size('page-single', 350, 350, true);
}

/**
 * Adds two classes to the array of body classes.
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 */
function sushi_body_classes( $classes ) {

	if ( function_exists( 'is_multi_author' ) && ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
		$classes[] = 'singular';

	return $classes;
}
add_filter( 'body_class', 'sushi_body_classes' );

// Really Simple Captcha Override
add_filter( 'wpcf7_use_really_simple_captcha', '__return_true' );


?>