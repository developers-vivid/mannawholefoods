<?php
/*
* Theme's footer area.
*
* @package WordPress
* @subpackage sushi
*/
global $sushi;
?>
<footer>
			<div class="main-container">
				<div class="sub-menu left">
					<?php wp_nav_menu( array( 'menu' => 'Footer One', 'container'    => '') ); ?>
					<?php wp_nav_menu( array( 'menu' => 'Footer Two', 'container'    => '') ); ?>
					<?php wp_nav_menu( array( 'menu' => 'Footer Three', 'container'    => '') ); ?>
					<?php wp_nav_menu( array( 'menu' => 'Footer Four', 'container'    => '') ); ?>
					<div class="clr"></div>
				</div>
				<div class="address right">
					<?php
				
						$address = get_post_meta(65,'address', true);
						$email = get_post_meta(65,'email', true);
						$facebook = get_post_meta(65,'facebook', true);
						$fax = get_post_meta(65,'fax', true);
						$landline = get_post_meta(65,'landline', true);
						$landlineunder = get_post_meta(65,'landlineunder', true);
						$content = get_post( $id = 65 );

						//thi is located on post under ID 65
					?>
					<ul id="footer-details-one">
						<li>Address:</li>
						<li><?php _e($address);?></li>
					</ul>
					<ul id="footer-details">
						<li><?php _e($landlineunder);?></li>
						<li><?php _e($fax);?></li>
						<!-- <li><a href="mailto:<?php _e($email);?>"><?php _e($email);?></a></li> -->
						<div class="clr"></div>
					</ul>
				</div>
				<div class="clr"></div>
				
			</div>
			<div class="clr"></div>
			<div class="footer-area">
				<div class="main-container">
					<div class="copyright left">
						<p>Copyright © <?php echo date("Y"); ?> <a href="<?php bloginfo( "home" ); ?>">Manna Wholefoods &amp; Café</a>. All Rights Reserved.</p>
					</div>
					<div class="branding right">
					<p><a href="http://www.sushidigital.com.au" target="_blank">Sushi Digital</a> <a href="http://www.sushidigital.com.au/services/website-design-development/" target="_blank">Website Design Perth</a></p>
					</div>
					<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</footer>
		<!-- /footer -->
		
	
</div>
<!-- /main-wrapper -->
<script type="text/javascript" src="<?php _e( $sushi->homeURL ); ?>/wp-content/plugins/contact-form-7/includes/js/scripts.js<?php _e( $sushi->getTimeSuffix() ); ?>"></script>
<script type='text/javascript' src='<?php _e( $sushi->homeURL ); ?>/wp-includes/js/jquery/jquery.form.js<?php _e( $sushi->getTimeSuffix() ); ?>'></script>
<script type="text/javascript" src="<?php _e( $sushi->templateURL ); ?>/js/global.js<?php _e( $sushi->getTimeSuffix() ); ?>"></script>

</body>
</html>
