<?php
/*
* Template Name: Vegetarian / Vegan
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 16 );
					$usefulwebsites = get_post_meta(16,'usefulwebsites', true);
				?>
				<h1><?php _e($content->post_title)?></h1>
				<?php
				
					$listhead = get_post_meta(16, 'listhead', true);
					$listcontent = get_post_meta(16, 'listcontent', true);
					$notice = get_post_meta(16, 'notice', true);

					$listhead = explode('|', $listhead);
					$listcontent = explode('|', $listcontent);
					
				if( !empty($listhead) && !empty($listcontent) ) : ?>
							<!-- <ul class="gourmet"> -->
							<?php _e($content->post_content);?>
							<?php //_e( apply_filters('the_content', $description) );
								foreach ($listhead as $key => $value) : ?>
									<p class="main-content-two"><?php _e('<strong>' .$value . '</strong><br />' . $listcontent[$key]) ?></p>

								<?php endforeach; ?>

							<!-- </ul> -->
				<?php endif; ?>
				<br class="clr">
				<!-- <ul class="main-content-three"><span class="title">Useful links:</span><br /><br />
					<li>Roar Vanilla: Information on superfoods, raw, living and vegan foods<br />
						<a href="#">http://www.roarvanilla.com</a>
					</li>
					
					<li>Happy Cow: Compassionate eating guide<br />
						<a href="<?php _e($usefulwebsites);?>"><?php _e($usefulwebsites);?></a>
						
					</li>
					<li>
					Go Veg!<br />
						<a href="<?php _e($usefulwebsites);?>"><?php _e($usefulwebsites);?></a>
					</li>
				</ul> -->
				
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>