<?php
/*
* Template Name: View all products
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
	$query = new WP_Query();
	
	$tpath = get_bloginfo( "template_url" );
	remove_filter( "the_content", "wpautop" );
	$content = apply_filters( "the_content", $data->post_content );

?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<h2><?php _e($data->post_title)?></h2>		
				<ul id="view-all-product">
				<?php
					$args = $query->query( array(
									'category_name'	=> 'featured',
									'post_type'		=> 'post',
									'post_status'	=> 'publish',
									'order'			=> 'date',
									'orderby'		=> 'DESC'	
					));
					
					foreach($args as $list):
						$image = get_post( get_post_thumbnail_id( $list->ID ) );
						$the_content = apply_filters( "the_content", $list->post_content );
						$url = $image->guid;
						$alt = get_post_meta( $list->ID, "Alt Text", true );
					?>
						<li>
							<img src="<?php set_timthumb( $tpath, $url, 272, 181 ); ?>" alt="<?php _e( $alt );?>" title="<?php _e( $alt ); ?>" class="left" />
							<?php _e( $the_content );?>
						</li>
						
					<?php	
					endforeach;
				?>
				</ul>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>