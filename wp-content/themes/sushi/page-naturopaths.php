<?php
/*
* Template Name: Naturopaths
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );

	remove_filter( "the_content", "wpautop" );
	$content = apply_filters( "the_content", $data->post_content );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 8 );
					$rheaplant = get_post_meta( 8,'rheaplant', true);
					$rheacontent = get_post_meta(8, 'rheacontent', true);
					//$contentlist = get_post_meta(103,'contentlist', false);
				?>
				<h1><?php _e($content->post_title)?></h1>
				
					<div class="main-content-two">
						<div class="inner-image-holder-narutopath">
						<?php
							$tpath = get_bloginfo( "template_url" );
							$image_url = get_post( get_post_thumbnail_id( 8 ) );
							$content = get_post( $id = 8);
						?>
						<img class="vacant" src="<?php set_timthumb( $tpath, $image_url->guid, 177, 239 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
					</div>
						<?php _e($content->post_content)?>
						<div class="clr"></div>
					</div>
					<br /><br />
					<div class="main-content-two">
						<?php
							$image = get_post( get_post_thumbnail_id( 120 ) );
							$alt = $image->post_title;
						?>
						<!--<img class ="rheaplant" src="<?php set_timthumb( $tpath, $rheaplant, 177, 239 ); ?>" alt="<?php _e( $alt );?>" title="<?php _e( $alt );?>"/>-->
					
						<?php _e( $rheacontent ); ?>
						<div class="clr"></div>
					</div>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>