<?php
/*
* Template Name: Contact Us
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );

	error_reporting(E_ALL & ~E_NOTICE);
	ini_set('display_errors', '1');
?>

	<section id="sub-content-area">
		<div class="main-container">	
			<!-- main-content -->
			<article class="main-entry round-border">
				<div class="main-content">
					<?php 
						if(have_posts()) :
							while(have_posts()): the_post();
								$map_image 	= get_field('map_image', $post->ID);
								$map_link 	= get_field('map_link', $post->ID);
								$address 	= get_field('address', $post->ID);
								$phone 		= get_field('phone', $post->ID);
								$fax 		= get_field('fax', $post->ID);
								$facebook 	= get_field('facebook', $post->ID);
								$contact_f 	= get_field('contact_form', $post->ID);

								$facebook = get_field('facebook', 1438);
								$instagram = get_field('instagram', 1438);

								$imgalt = get_post_meta( $map_image['id'] ,'_wp_attachment_image_alt',true);
								?>
								<a href="<?php echo $map_link ?>" rel="nofollow" target="_blank"><img src="<?php echo $map_image['url']  ?>" title="<?php echo $imgalt ?>" alt="<?php echo $imgalt ?>" width="831"/></a>
								<div class="content-sub">
									<ul>
										<li>Address: <?php echo $address ?></li>
										<li>Phone: <?php echo $phone ?></li>
										<li>Fax: <?php echo $fax ?></li>
										<!-- <li><a href="http://<?php echo $facebook ?>" target="_blank" rel="nofollow"><img src="<?php bloginfo('template_url') ?>/images/facebook.png" title="like us on facebook" alt="like us on facebook"/></a></li> -->
									</ul>
									
								</div>
								<div class="social-media contact-us clearfix">
									<?php if ( $instagram || $facebook ): ?>
									<ul>
										<?php echo ( $facebook ? '<li><a href="'.$facebook.'" target="_blank"><img src="'.get_template_directory_uri().'/images/fb-icon.png" title="Like us on Facebook"></a></li>' : '' ); ?>
										<?php echo ( $instagram ? '<li><a href="'.$instagram.'" target="_blank"><img src="'.get_template_directory_uri().'/images/ig-icon.png" title="Like us on Instagram"></a></li>' : '' ); ?>
									</ul>
									<?php endif ?>
								</div>
								<?php
							endwhile;
						endif;
					?>
				</div>
			</article>
			<?php get_sidebar(); ?>
			<div class="clr"></div>
			<article class="contact-us-sub">
				<div class="contact-us-sub-con">
					<?php 
						if(have_posts()) :
							while(have_posts()): the_post();
								$contact_f 	= get_field('contact_form', $post->ID);
								echo do_shortcode( $contact_f );
							endwhile;
						endif;
					?>
				</div>
			</article>
			<!-- /main-content -->
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->

<?php get_footer(); ?>