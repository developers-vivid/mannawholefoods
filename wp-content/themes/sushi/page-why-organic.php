<?php
/*
* Template Name: Why Organic
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 20 );
					$usefulwebsites = get_post_meta(20, 'usefulwebsites', true);
				?>
				<h1><?php _e($content->post_title)?></h1>
				<p class="main-content-two">
					<?php _e($content->post_content)?>

					<!-- <a href="<?php _e($usefulwebsites)?>"><?php _e($usefulwebsites)?></a> -->
					<div class="clr"></div>
					<?php
				
					$listhead = get_post_meta(20, 'listhead', true);
					$listcontent = get_post_meta(20, 'listcontent', true);
					$notice = get_post_meta(20, 'notice', true);

					$listhead = explode('|', $listhead);
					$listcontent = explode('|', $listcontent);
					
				if( !empty($listhead) && !empty($listcontent) ) : ?>
							<ul class="main-content-three">
							
							<?php //_e( apply_filters('the_content', $description) );
								foreach ($listhead as $key => $value) : ?>
									<li><?php _e('<span class="title">'.$value . '</span><br />' . $listcontent[$key]) ?></li>

								<?php endforeach; ?>
		
							</ul>
						<?php endif; ?>
					
				</p>
				
				<div class="clr"></div>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>