<?php
/*
* Sidebar section.
*
* @package WordPress
* @subpackage sushi
*/

?>
<?php
	global $wpdb;
	global $sushi;
	$pageid = get_id();
	$data = get_page( $pageid );
	$custom_posts = get_posts( array(	"post_status" => "publish",'post_type' => 'page',"posts_per_page" => -1, "post_parent" => $pageid	) );

	$tpath = get_bloginfo( "template_url" );
	remove_filter( "the_content", "wpautop" );
	$content = apply_filters( "the_content", $data->post_content );
?>
<div id="control-sidebar">
	<div class="block right">
		<div class="block-wrapper">
			<div class="title-control">
				
				<h5>Latest News</h5>
				<?php
					$title = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$title =  new WP_Query( array('posts_per_page' => -1, 'category_name' => 'article-recipes', 'order' => 'desc', 'post_status' => 'publish', 'post_type' => 'post','orderby' => 'date' ) );
					
					
				?>
				<?php while ($title->have_posts()) : $title->the_post(); ?>
					
				<?php 
					$pid = get_the_id();
					if( $pageid == $pid ){
						$cur_link = 'curbold';
						
					}
					else{
						$cur_link = '';
					}
				?>
				<div class="per-title">
					<a href="<?php echo get_permalink(); ?>" class="<?php echo $cur_link; ?>"><?php the_title(); ?></a>
				</div>
				<?php endwhile; ?>
			</div>
		<br />
		</div>
		<div class="clr"></div>
	</div>
	<div class="block right">
		<div class="block-wrapper">
			<?php //mailchimpSF_signup_form(); ?>
			<script type="text/javascript">
				jQuery( function($) {

					$( "#mc_mv_FNAME" ).attr( "placeholder", 'First Name'  );
					$( "#mc_mv_EMAIL" ).attr( "placeholder", 'Email'  );
					$( "#mc_signup_submit" ).val( "SUBMIT");

				});

			</script>
		</div>
		<div class="clr"></div>
	</div>
	<div class="clr"></div>
</div>
