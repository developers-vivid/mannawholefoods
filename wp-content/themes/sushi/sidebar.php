<?php
/*
* Sidebar section.
*
* @package WordPress
* @subpackage sushi
*/
global $sushi;
?>
<div id="control-sidebar">
	<div class="block right">
		<div class="block-wrapper">
			<?php
				$content = get_post( $id = 85 );
				$contentlist = get_post_meta(85,'contentlist', false);
				// $openinghours = get_post_meta(85,'openinghours', false);

				$opening_hours = get_field('opening_hours', 1438 );
				$public = get_field('public', 1438 );
			?>
			<p class="title">Opening Hours</p>

			<?php if ( $opening_hours ): ?>
			<?php foreach ($opening_hours as $key => $value): ?>
				<ul id="schedule">
					<?php echo ( $value ? '<li><span>'.$value['days'].'<br/></span>'.$value['opening_time'].'</li>' : '' ); ?>
				<div class="public-holidays">
					<?php foreach ($value['note'] as $key => $value): ?>
						<?php echo ( $value['content'] ? $value['content'].'<br/>' : '' ); ?>
					<?php endforeach ?>
				</div>
				</ul>
			<?php endforeach ?>
			<?php endif ?>
			<!-- <ul id="schedule">
				<?php
					//echo ( $opening_hours ? '<li>'.$opening_hours.'</li>' : '' );
					// print_r($openinghours);
					// foreach ($openinghours as  $value) {
					// 	echo '<li>'.$value.'</li>';
					// }
				?>
				<?php

					// $public = get_post_custom_values('close-holidays',85);
				?>
				<div class="public-holidays">
				<?php //cho ( $public ? $public : '' ); ?>
				<?php //_e( $public[0] );?>
					
				</div>
			</ul> -->
		</div>
		<div class="clr"></div>
	</div>
	<div class="block right">
		<div class="block-wrapper">
			<?php //mailchimpSF_signup_form(); ?>
			<script type="text/javascript">
				jQuery( function($) {

					$( "#mc_mv_FNAME" ).attr( "placeholder", 'First Name'  );
					$( "#mc_mv_EMAIL" ).attr( "placeholder", 'Email'  );
					$( "#mc_signup_submit" ).val( "SUBMIT");

				});

			</script>
		</div>
		<div class="clr"></div>
	</div>
	<div class="featured right">
	<div class="ribbon"></div>
		<?php
			$args = array(
					'category' => 12,
					'orderby' => 'rand'
				);
			$image = '';
			$randompost = get_posts($args);

				if ( has_post_thumbnail( $randompost[0]->ID ) )
				{
					// featured image
					$image = get_post( get_post_thumbnail_id( $randompost[0]->ID ) );
					// ensure image path only
					$image_url = $image->guid;

				}
				
				$tpath = get_bloginfo( "template_url" );

		?>
	<img src="<?php set_timthumb( $tpath, $image_url, 258, 292 ); ?>" alt="<?php echo $image->post_title;?>" title="<?php echo $image->post_title; ?>"/>
	<p class="caption-featured">
		<?php $links = get_post_custom_values('link', $randompost[0]->ID );?>
		<a href="<?php  echo get_permalink($randompost[0]->ID); ?>" class="access"><?php _e($randompost[0]->post_title);?></a><br />
		<?php //_e($randompost[0]->post_excerpt );?>
	</p>
	
	<div class="clr"></div>
	</div>
	<div class="clr"></div>
</div>