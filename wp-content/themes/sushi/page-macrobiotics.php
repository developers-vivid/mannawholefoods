<?php
/*
* Template Name: Macrobiotics
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );

	remove_filter( "the_content", "wpautop" );
	$content = apply_filters( "the_content", $data->post_content );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 22 );
					//$contentlist = get_post_meta(103,'contentlist', false);
					$subcontent = get_post_meta(22, 'subcontent', true);
					$subcontent2 = get_post_meta(22, 'subcontent2', true);

				?>
				<h1><?php _e($content->post_title)?></h1>
				
					<div class="main-content-two">
						<div class="inner-image-holder-macrobiotics">
							<?php
								$tpath = get_bloginfo( "template_url" );
								$image_url = get_post( get_post_thumbnail_id( 22 ) );
								$content = get_post( $id = 22);
							?>
							<img src="<?php set_timthumb( $tpath, $image_url->guid, 425, 282 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>" />
						</div>
						<?php _e($content->post_content)?><br /><br />

					</div>
					<div class="clr"></div>
					<p class="main-content-two"><?php _e($subcontent)?><br /><br /><div class="clr"></div></p>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>