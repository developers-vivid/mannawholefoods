<?php
/*
* 404
*
* @package WordPress
* @subpackage sushi
*/
?>
<?php get_header(); ?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
			
				<div class="page-error">
					<h5>Error 404: Page Not Found!</h5>
					<p>Oops! There's seems to be a problem. The page you're trying to view does not exist.</p>
				</div>
			
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
	
<?php get_footer(); ?>