<?php
/*
* Template Name: The Shop
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>

	<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 5 );
					//$contentlist = get_post_meta(103,'contentlist', false);
				?>
				<h1><?php _e($content->post_title)?></h1>
				<div class="inner-image-holder">
					<?php
						$tpath = get_bloginfo( "template_url" );
						$image_url = get_post( get_post_thumbnail_id( 5 ) );
						$content = get_post( $id = 5);
					?>
					<img src="<?php set_timthumb( $tpath, $image_url->guid, 789, 284 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
				</div>
				<?php
				
					$listhead = get_post_meta(5, 'listhead', true);
					$listcontent = get_post_meta(5, 'listcontent', true);
					$notice = get_post_meta(5, 'notice', true);

					$listhead = explode('|', $listhead);
					$listcontent = explode('|', $listcontent);
					
				if( !empty($listhead) && !empty($listcontent) ) : ?>
							<ul class="gourmet">
							<?php _e($content->post_content);?>
							<?php //_e( apply_filters('the_content', $description) );
								foreach ($listhead as $key => $value) : ?>
									<li><p><?php _e('<span>'.$value . '</span><br />' . $listcontent[$key]) ?></p></li>

								<?php endforeach; ?>

							</ul>
						<?php endif; ?>
					<p class="main-content"><?php _e($notice);?></p>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
			<div class="clr"></div>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->

<?php get_footer(); ?>