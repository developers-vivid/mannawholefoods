<?php
/*
* Template Name: Raw Foods
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<div class="main-content">
				<?php
					$content = get_post( $id = 12 );
					//$contentlist = get_post_meta(103,'contentlist', false);
				?>
				<h1><?php _e($content->post_title)?></h1>
					<div class="inner-image-holder">
						<?php
							$tpath = get_bloginfo( "template_url" );
							$image_url = get_post( get_post_thumbnail_id( 12 ) );
							$content = get_post( $id = 12);
						?>
						<img src="<?php set_timthumb( $tpath, $image_url->guid, 789, 284 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
					</div>
				
					<p class="main-content-two"><?php _e($content->post_content)?></p>

					<?php $listcontent = get_post_meta(12, 'listcontent', true); ?>
						<ul class="gourmet-sub">
							<?php _e($listcontent) ?>
						</ul>
				</div>
				<div class="clr"></div>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>