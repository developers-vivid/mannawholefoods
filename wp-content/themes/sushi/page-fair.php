<?php
/*
* Template Name: Fair Trade
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="sub-content-area">
		<div class="main-container">		
			<article class="main-entry round-border">
				<?php
					$content = get_post( $id = 14 );
					$usefulwebsites = get_post_meta(14,'usefulwebsites', true);
					$usefulwebsites2 = get_post_meta(14,'usefulwebsites2', true);
					//$contentlist = get_post_meta(103,'contentlist', false);
				?>
				<h1><?php _e($content->post_title)?></h1>
				<div class="main-content">
					
					<p class="main-content-two">
						
							<?php
								$tpath = get_bloginfo( "template_url" );
								$image_url = get_post( get_post_thumbnail_id( 14 ) );
								$content = get_post( $id = 14);
							?>
							<img src="<?php set_timthumb( $tpath, $image_url->guid, 128, 149 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>" class="image-control"/>
						
						<?php _e($content->post_content)?>

						<a href="http://<?php _e($usefulwebsites);?>" target="_blank" class="green" ><?php _e($usefulwebsites);?></a><br />
						<a href="http://<?php _e($usefulwebsites2);?>" target="_blank" class="green"><?php _e($usefulwebsites2);?></a>
					</p>
				</div>
				<div class="clr"></div>
			</article>
			<!-- /main-content -->
			<?php get_sidebar(); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->
<?php get_footer(); ?>