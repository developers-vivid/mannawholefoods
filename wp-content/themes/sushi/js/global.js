/* 
* GLOBAL TOOLS AND UTILITIES SCRIPTS
* Place all custom js/jquery scripts here.
*
*/

/* Initialize and/or execute, code jQuery Scripts here */
jQuery( function($) {
	
	/* Initialize KaiTools */
	$( document ).KaiTools({
		placeholder: true
	});

	$( "#rotator" ).slides({
		container: 'rotator-wrapper',
		preload: false,
		play: 5000,
		pause: 2500,
		hoverPause: true
	});
	
	$( ".wpcf7-submit" ).click( function(e) {
		$( ".wpcf7-response-output" ).delay( 500 ).queue( function() {
			var $msg = $( this );
			if ( $msg.length )
			{
				$msg.delay( 4000 ).fadeOut( 500, function() {
									
				});
			}
			$( this ).dequeue();
		});
	});
		
	

    // Contact Form 7 Events
	$( ".wpcf7-form" ).ajaxComplete( function( event, request, settings ) {
		//var $cf7 = $( event.target );
		
		//$cf7.find( '.wpcf7-response-output' ).delay( 4000 ).fadeOut( 500 );
		$( '.wpcf7-response-output' ).delay( 4000 ).fadeOut( 500 );
	});	
	
	$(document).on('mouseover', "span.wpcf7-not-valid-tip", function (e) {
        $(this).fadeOut('fast', function () {
            if ($(this).prev().length) {
                $(this).prev().focus();
            }
        });
    });

    $(document).on('focus', ".wpcf7 input[type='text'], .wpcf7 textarea", function () {
        if ($(this).next().length) {
            $(this).next().fadeOut('fast');
        }
    });

    // Set phone validation on a field.
	validate_phone( ".phone-input" );

	function validate_phone( selector ) {
		$( selector ).keydown( function( event ) {
			// Allow: backspace, delete, tab, escape, and enter
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				 // Allow: Ctrl+A Select All
				(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: Ctrl+V Paste
				(event.keyCode == 86 && event.ctrlKey === true) ||
				 // Allow: home, end, left, right
				(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			else {
				// Ensure that it is a number and stop the keypress
				if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		});
	}
	
});


/*
* END OF FILE
* global.js
*/