<?php
/*
* Theme's Home Template
* Template Name: Home
*
* @package WordPress
* @subpackage Sushi
*/
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
?>
<section id="content-area">
			<div class="main-container">
				<div class="slider-holder left">
					<div id="rotator" class="round-border">
						<div class="rotator-wrapper">
							<?php
								$tpath = get_bloginfo( "template_url" );
								$slider_query = new WP_Query();
								// get posts in 'slider' category
								$sliders = $slider_query->query( array(
									"category_name" => "slider",
									"orderby" => "ID",
									"order" => "ASC",
									"post_type" => "post",
									"post_status" => "publish"
								));
								
								if ( $sliders ) :
								
									foreach ( $sliders as $slider ) :
									
										if ( has_post_thumbnail( $slider->ID ) )
										{
											// featured image
											$image = get_post( get_post_thumbnail_id( $slider->ID ) );
											// ensure image path only
											$image = $image->guid;
										}
										else
										{
											/* $image = alternate image if no thumbnail is found */
										}
										
										// title / heading
										$title = $slider->post_title;
										// description
										$desc = apply_filters( "the_content", $slider->post_content );
										// alternate text from custom field "Alt Text"
										$alttext = get_post_meta( $slider->ID, "Alt Text", true );						
									
							?>

								<div class="slide">
									<img src="<?php set_timthumb( $tpath, $image, 833, 510 ); ?>" alt="<?php echo $alttext; ?>" title="<?php echo $title; ?>" />
								</div>
								

						<?php 	endforeach; ?>
					<?php endif; ?>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<?php get_sidebar(); ?>

				<article class="main-entry">
					<?php
						$content = get_post( $id = 85 );
						$subcontent = get_post_meta(85,'subcontent', true);
					?>
					<h1>
						<?php _e( $content->post_title );?>
					</h1>
					<p class="main-content">
						<?php _e( $content->post_content )?>
					</p>
					<p class="main-content-two"><?php _e( $subcontent );?></p>
				</article>
				<!-- /main-content -->
				<br class="clr" />
				<div id="content-bottom">
					<h2 class="featured-products">Featured Products</h2>
					<a href="#" class="views right">View All</a>
					<div class="clr"></div>
				</div>
					<ul id="featured-images">
						<?php

							$tpath = get_bloginfo( "template_url" );

							$featured_query = new WP_Query();
							$featured = $featured_query->query( array(
									"category_name" => "featured",
									"orderby" => "ID",
									"order" => "ASC",
									"post_type" => "post",
									"post_status" => "publish"
								));

							if ( $featured ) :
								
									foreach ( $featured as $featured ) :
									
										if ( has_post_thumbnail( $featured->ID ) )
										{
											// featured image
											$image = get_post( get_post_thumbnail_id( $featured->ID ) );
											// ensure image path only
											$image = $image->guid;
										}
										else
										{
											/* $image = alternate image if no thumbnail is found */
										}
										
										// title / heading
										$title = $featured->post_title;
										// description
										$desc = apply_filters( "the_content", $featured->post_content );
										// alternate text from custom field "Alt Text"
										$alttext = get_post_meta( $featured->ID, "Alt Text", true );						
									
							?>
					
						<li>
							<img src="<?php set_timthumb( $tpath, $image, 272, 181 ); ?>" alt="<?php echo $alttext; ?>" title="<?php echo $title; ?>" />
							<p><?php _e( $desc );?></p>
						</li>
						<?php 	endforeach; ?>
					<?php endif; ?>

					</ul>
				<div class="articles left">
					<h3>Latest Articles</h3>
					<div class="wrapper">
						<?php
							$content = get_post( $id = 103 );
							$contentlist = get_post_meta(103,'contentlist', false);
							


						?>
						<p class="intro"><?php _e( $content->post_title );?></p>
						<p><?php _e( $content->post_content );?></p>
						<ul>
							<?php
								foreach ($contentlist as  $value) {
									echo '<li><a href="#">'.$value.'</a></li>';
								}
							?>
							
							
						</ul>
					</div>
				</div>
				<div class="resident right">
					<h3>Resident Naturopath</h3>
					<div class="wrapper">
						<div class="image-holder left">
							<?php
								$tpath = get_bloginfo( "template_url" );
								$image_url = get_post( get_post_thumbnail_id( 120 ) );
								$content = get_post( $id = 120);
								$landline2 = get_post_meta(120,'landline2',true);
							?>
							<img src="<?php set_timthumb( $tpath, $image_url->guid, 177, 239 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
						</div>
						<p>
							<span><?php _e( $content->post_title );?></span><br />
							<?php _e( $content->post_content );?>
						</p>
						<div class="direct-contact">
							<div class="information"><p>
								Call<br />
								<span class="information-number"><?php _e($landline2)?></span><br />
								to make appointment
							</p></div>
							<div class="clr"></div>
						</div>
						<div class="clr"></div>
					</div>
				</div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</section>
		<div class="clr"></div>
		<!-- /content-area -->
<?php get_footer(); ?>