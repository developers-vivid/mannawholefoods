<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage sushi
 */
?>
<?php get_header(); ?>
<?php
	global $wpdb;
	$pageid = get_id();
	$data = get_page( $pageid );
	$tpath = get_bloginfo( "template_url" );

	remove_filter( "the_content", "wpautop" );
	$content = apply_filters( "the_content", $data->post_content );
?>
<section id="sub-content-area">
		<div class="main-container">
		<?php
			if(is_single(745) ){
			?>
			<article class="main-entry round-border">
				<div class="content">									

					<h1><?php _e($data->post_title)?></h1>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="per-post-single">
						<div class="main-content-four-sub">
							<?php if ( has_post_thumbnail() ) { ?>
									<div class="image-holder-news">
										<?php						
										$image_url = get_post( get_post_thumbnail_id( get_the_ID() ) );
										$content = get_the_content();
										$sub_text = get_post_custom_values('sub-text', get_the_ID );
									?>
										<h3><?php _e($sub_text[0] );?></h3>
										<img src="<?php set_timthumb( $tpath, $image_url->guid,790, 667,100, 3); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
									</div>	
							<?php } ?>
								
								<div class="clr"></div>
								
								<?php _e($content);?>
						</div>
						<div class="clr"></div>
					</div>
					<?php endwhile; ?>
				</div>
			</article>
			<?php	
			}else if(is_single( 741 ) ){
			?>
			<article class="main-entry round-border">
				<div class="content">									

					<h1><?php _e($data->post_title)?></h1>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="per-post-single">
						<div class="main-content-four-sub">
							<?php if ( has_post_thumbnail() ) { ?>
									<div class="image-holder-news">
										<?php						
										$image_url = get_post( get_post_thumbnail_id( get_the_ID() ) );
										$content = get_the_content();
										
									?>
										<img src="<?php set_timthumb( $tpath, $image_url->guid,372,400,100, 3); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>"/>
									</div>	
									
							<?php } ?>
								<div class="clr"></div>
								
								<?php _e($content);?>
						</div>
						<div class="clr"></div>
					</div>
					<?php endwhile; ?>
				</div>
			</article>
			<?php	
			}else if(is_single(90) ){
			?>
				<article class="main-entry round-border">
					<div class="content">									

						<h1><?php _e($data->post_title)?></h1>
						<?php while ( have_posts() ) : the_post(); ?>
						<div class="per-post-single">
							<div class="main-content-four-sub">
								<?php if ( has_post_thumbnail() ) { ?>
											<?php						
											$image_url = get_post( get_post_thumbnail_id( get_the_ID() ) );
											$content = get_the_content();
											
											?>
											<!--<img src="<?php set_timthumb( $tpath, $image_url->guid,200, 0 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>" class="right" />-->
										
								<?php } ?>
									
										<?php _e($content); ?>
									<div class="clr"></div>
							</div>
							<div class="clr"></div>
						</div>
						<?php endwhile; ?>
					</div>
				</article>
			<?php	
			}else if(is_single( 1020 )){
			?>
				<article class="main-entry round-border">
					<div class="content">									

						<h1><?php _e($data->post_title)?></h1>
						<?php while ( have_posts() ) : the_post(); ?>
						<div class="per-post-single">
							<div class="main-content-four-sub">
								<?php if ( has_post_thumbnail() ) { ?>
											<?php						
											$image_url = get_post( get_post_thumbnail_id( get_the_ID() ) );
											$content = get_the_content();
											
											?>
											<!--<img src="<?php set_timthumb( $tpath, $image_url->guid,200, 0 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>" class="right" />-->
										
								<?php } ?>
									
										<?php _e($content); ?>
									<div class="clr"></div>
							</div>
							<div class="clr"></div>
						</div>
						<?php endwhile; ?>
					</div>
				</article>
			<?php	
			}else{
			?>
				<article class="main-entry round-border">
				<div class="content">									

					<h1><?php _e($data->post_title)?></h1>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="per-post-single">
						<div class="main-content-four-sub">
							<?php if ( has_post_thumbnail() ) { ?>
										<?php						
										$image_url = get_post( get_post_thumbnail_id( get_the_ID() ) );
										$content = get_the_content();
										
										?>
										<img src="<?php set_timthumb( $tpath, $image_url->guid,200, 0 ); ?>" alt="<?php echo $image_url->post_title;?>" title="<?php echo $image_url->post_title; ?>" class="right" />
									
							<?php } ?>
								
									<?php _e($content); ?>
								<div class="clr"></div>
						</div>
						<div class="clr"></div>
					</div>
					<?php endwhile; ?>
				</div>
			</article>
			<?php
			}
		?>	
			
			<!-- /main-content -->
			<?php get_sidebar('left'); ?>
		</div>
		<div class="clr"></div>
	</section>
	<div class="clr"></div>
	<!-- /content-area -->


<?php get_footer(); ?>