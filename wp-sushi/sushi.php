<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * A library that contains wrapper and helper classes and methods
 * to simplify or shorten Wordpress functions and make it easier to utilize
 * and extend.
 *
 * This library intends to provide developers with versatile tools to
 * shorten the coding time with efficient data output.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

define( 'LIB_ABSPATH', dirname( __FILE__ ) );

require( LIB_ABSPATH . '/settings.php' );


add_action( 'init', 'swp_init' );

/*
* END OF FILE
* sushi.php
*/
?>