<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * Configuration file.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

require( LIB_ABSPATH . '/default-constants.php' );
require( LIB_ABSPATH . '/functions.php' );
require( LIB_ABSPATH . '/version.php' );

// Sushi Wordpress Starter version model.
define( 'SWP_SYSTEM_MODEL', $system['model'] );

// Sushi Wordpress Starter version number.
define( 'SWP_SYSTEM_VERSION', $system['version'] );

// Sushi Wordpress Starter 3.0 default admin theme.
define( 'SWP_STARTER_DEFAULT_ADMINTHEME', 'sashimi' );

// Basic setting only
define( 'SWP_PACKAGE', 'basic' );

// Library root directory and URL.
define( 'LIBRARY_URL', home_url( '/' ) . 'wp-sushi' );

swp_library_dir_constants();
swp_admin_constants();
swp_options_constants();

require( LIB_ABSPATH . '/load.php' );

?>