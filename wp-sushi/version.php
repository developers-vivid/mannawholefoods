<?php
/**
 * Sushi WordPress Starter System Library
 *
 * Starter Version
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
 
require( ABSPATH . WPINC . '/version.php' );
 
$system = array(
	'model' 		=> 'Sashimi',
	'version' 		=> '3.0.1',	
	'wp_version' 	=> $wp_version
);

/*
* END OF FILE
* version.php
*/ 
?>