<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * Wordpress Media library object wrapper class.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 *
 */
class SWP_Media
{	
	public $ID;
	public $author;
	public $date;
	public $dateGmt;
	public $description;
	public $title;
	public $caption;
	public $status;
	public $name;
	public $url;
	public $type = 'attachment';
	public $mimeType;
	public $alt;
	public $width;
	public $height;
	public $fileSize;
	public $fileExt;
	public $metadata;
	
	public function __construct( $wp_post )
	{
		if ( get_class( $wp_post ) !== 'WP_Post' ) {
			echo sprintf( '<p class="swp-error">Argument $wp_post is invalid: must be a WP_Post object.</p>' );
			return;
		}
		
		$this->type = 'attachment';
		
		if ( !in_array( $wp_post->post_type, array( 'page', 'post', 'attachment' ) ) ) {
			echo sprintf( '<p class="swp-error">Argument post_type (\'%s\') property is invalid: Accepts \'post\', \'page\', and \'attachment\' post types only.</p>', $wp_post->post_type );
			return;
		}
		
		if ( $wp_post->post_type !== 'attachment' ) {
			
			$thumb_id = get_post_meta( $wp_post->ID, '_thumbnail_id', true );
			$wp_post = get_post( $thumb_id );
		}
		
		$this->ID = $wp_post->ID;
		$meta = (object)get_post_meta( $this->ID, '_wp_attachment_metadata', true );
		$this->author = $wp_post->post_author;
		$this->date = $wp_post->post_date;
		$this->dateGmt = $wp_post->post_date_gmt;
		$this->description = $wp_post->post_content;
		$this->title = $wp_post->post_title;
		$this->caption = $wp_post->post_excerpt;
		$this->status = $wp_post->post_status;
		$this->name = $wp_post->post_name;
		$this->url = $wp_post->guid;		
		$this->mimeType = $wp_post->post_mime_type;
		$this->alt = get_post_meta( $this->ID, '_wp_attachment_image_alt', true );		
		$this->width = $meta->width;
		$this->height = $meta->height;
		$this->fileSize = filesize( swp_upload_dir( 'basedir' ) . '/' . $meta->file );
		$this->fileExt = pathinfo( swp_upload_dir( 'basedir' ) . '/' . $meta->file, PATHINFO_EXTENSION );
		$this->metadata = $meta;
	}	
}

/*
* END OF FILE
* class.swp-media.php
*/
?>