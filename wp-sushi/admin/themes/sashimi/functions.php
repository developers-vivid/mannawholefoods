<?php
/**
 * Sushi Worpdress Starter System
 *
 * Sashimi theme functions.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

function swp_admintheme_init()
{
	swp_add_css( 'sushi-admintheme-dashboard', SWP_CURRENTTHEME_URL . '/css/dashboard.css' );
	swp_add_js( 'sushi-doll-talk', SWP_CURRENTTHEME_URL . '/js/jquery.sushidolltalk.js', array( 'jquery', 'sushi-plugins' ) );	
	
	if ( is_wp_login_page() )
	{
		swp_load_js( 'sushi-doll-talk' );
		swp_admintheme_login_init();
	}	
	
}

function swp_admintheme_login_init()
{
	add_filter( 'login_speech', 'swp_admintheme_filter_speech' );
	add_filter( 'login_message', 'swp_admintheme_filter_message' );
	add_filter( 'login_errors', 'swp_admintheme_filter_errors' );
	add_filter( 'login_messages', 'swp_admintheme_filter_messages' );
	
	// prints and loads scripts
	add_action( 'swp_login_scripts', 'swp_admintheme_load_login_scripts' );	
}

function swp_admintheme_load_login_scripts()
{	
	echo create_favicon_link( get_template_directory_uri() . '/favicon.ico' ) . "\n";
	echo ie_conditional( 'lt', 9, '<script src="' .  SWP_ASSETS_URL . '/scripts/html5shiv.js"></script>
<style type="text/css">
	* { *behavior: url( "' .  SWP_ASSETS_URL . '/scripts/boxsizing.htc" ); }
</style>' );
}

function swp_admintheme_get_image_src( $file, $force_echo = false )
{	
	$path = SWP_CURRENTTHEME_URL . '/images';
	$image = $path . '/' . $file;

	if ( $force_echo )
		echo $image;

	return $image;
}

/**
 * Filters speech outputs.
 *
 * @param string $speech The speech to filter
 */
function swp_admintheme_filter_speech( $speech )
{
	$speech = '<p class="balloon single"><span>' . $speech . '</span></p>';	

	return $speech . "\n";
}

/**
 * Filters message outputs.
 *
 * @param string $message The message to filter
 */
function swp_admintheme_filter_message( $message )
{
	if ( empty( $message ) )
		return null;

	$message = '<p class="balloon wp-msg"><span>' . strip_tags( $message, '<a><em><strong>' ) . '</span></p>';

	return $message . "\n";
}

/**
 * Filters error outputs.
 *
 * @param string $errors The errors to filter
 */
function swp_admintheme_filter_errors( $errors )
{
	$errors = '<p class="balloon error"><span>' . str_replace( array( '<strong>ERROR</strong>: ', '<a' ), array( '', '<br /><a' ), $errors ) . '</span></p>';

	return $errors . "\n";
}

/**
 * Filters messages.
 *
 * @param string $messages The messages to filter
 */
function swp_admintheme_filter_messages( $messages )
{
	$messages = '<p class="balloon msg"><span>' . $messages . '</span></p>';

	return $messages . "\n";
}

?>