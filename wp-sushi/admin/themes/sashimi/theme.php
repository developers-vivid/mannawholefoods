<?php
/**
 * Sushi Worpdress Starter System
 *
 * Sashimi theme configuration file.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

$admin_theme = array(
	'id'			=> 'sashimi',
	'name'			=> 'Sashimi',
	'description'	=> 'Default back-end theme of Sushi Wordpress Starter 3.0.',
	'author'		=> 'Karl Vincent Init',
	'author_url'	=> 'http://facebook.com/kai.zard/',
	'version'		=> '1.0.1'
);

$GLOBALS['syslib']['admin_theme'] = $admin_theme;

define( 'SWP_CURRENTTHEME_DIR', SWP_ADMINTHEMES_DIR . '/' . $admin_theme['id'] );
define( 'SWP_CURRENTTHEME_URL', SWP_ADMINTHEMES_URL . '/' . $admin_theme['id'] );

require( SWP_CURRENTTHEME_DIR .  '/functions.php' );

add_action( 'swp_admintheme_init', 'swp_admintheme_init' );

?>